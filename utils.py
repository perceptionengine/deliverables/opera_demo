import cv2
import yaml
import math
import numpy as np
import open3d as o3d


def yolo_nuscenes_class_map():
    yolo_to_nuscenes = [
    'vehicle.car',
    'vehicle.truck',
    'vehicle.bus.rigid',
    'vehicle.trailer',
    'vehicle.motorcycle',
    'vehicle.bicycle',
    'human.pedestrian.adult',
    'human.pedestrian.child',
    'human.pedestrian.construction_worker',
    'static.manmade',
    'movable_object.trafficcone',
    'movable_object.pushable_pullable',
    'movable_object.barrier',
    'vehicle.construction',
    'vehicle.emergency.ambulance',
    'static_object.bicycle_rack',
    'animal',
    'movable_object.debris',
    'human.pedestrian.personal_mobility',
    'vehicle.emergency.police',
    'human.pedestrian.police_officer',
    'undesirable_object',
    ]
    return yolo_to_nuscenes

def get_colormap():
    """
    Get the defined colormap.
    :return: A mapping from the class names to the respective RGB values.
    """
    classname_to_color = {  # RGB.
        "noise": (0, 0, 0),  # Black.
        "animal": (70, 130, 180),  # Steelblue
        "human.pedestrian.adult": (0, 0, 230),  # Blue
        "human.pedestrian.child": (135, 206, 235),  # Skyblue,
        "human.pedestrian.construction_worker": (100, 149, 237),  # Cornflowerblue
        "human.pedestrian.personal_mobility": (219, 112, 147),  # Palevioletred
        "human.pedestrian.police_officer": (0, 0, 128),  # Navy,
        "human.pedestrian.stroller": (240, 128, 128),  # Lightcoral
        "human.pedestrian.wheelchair": (138, 43, 226),  # Blueviolet
        "movable_object.barrier": (112, 128, 144),  # Slategrey
        "movable_object.debris": (210, 105, 30),  # Chocolate
        "movable_object.pushable_pullable": (105, 105, 105),  # Dimgrey
        "movable_object.trafficcone": (47, 79, 79),  # Darkslategrey
        "static_object.bicycle_rack": (188, 143, 143),  # Rosybrown
        "vehicle.bicycle": (220, 20, 60),  # Crimson
        "vehicle.bus.bendy": (255, 127, 80),  # Coral
        "vehicle.bus.rigid": (255, 69, 0),  # Orangered
        "vehicle.car": (255, 158, 0),  # Orange
        "vehicle.construction": (233, 150, 70),  # Darksalmon
        "vehicle.emergency.ambulance": (255, 83, 0),
        "vehicle.emergency.police": (255, 215, 0),  # Gold
        "vehicle.motorcycle": (255, 61, 99),  # Red
        "vehicle.trailer": (255, 140, 0),  # Darkorange
        "vehicle.truck": (255, 99, 71),  # Tomato
        "flat.driveable_surface": (0, 207, 191),  # nuTonomy green
        "flat.other": (175, 0, 75),
        "flat.sidewalk": (75, 0, 75),
        "flat.terrain": (112, 180, 60),
        "static.manmade": (222, 184, 135),  # Burlywood
        "static.other": (255, 228, 196),  # Bisque
        "static.vegetation": (0, 175, 0),  # Green
        "vehicle.ego": (255, 240, 245)
    }
    return classname_to_color

## CONVERSION FUNCTIONS
def Yolo2BBox(image, yolo_labels):
    # YOLO labels format is (class, x_center_normalized, y_center_normalized, width_normalized, height_normalized)
    # where normalization is based on image size. Here we convert to pixel coordinates (x, y, width, height)
    bbox = np.empty((yolo_labels.shape[0], 4))
    classes = []
    width = image.shape[1]
    height = image.shape[0]
    if yolo_labels.shape[1] > 0:
        for i, label in enumerate(yolo_labels):
            x_center = label[1] * width
            y_center = label[2] * height
            obj_width = label[3] * width
            obj_height = label[4] * height
            bbox[i, 0], bbox[i, 1] = x_center, y_center
            bbox[i, 2], bbox[i, 3] = obj_width, obj_height
            classes.append(yolo_nuscenes_class_map()[int(label[0])])
    return bbox, classes


def DepthImage2Pointcloud(depth_image, cam_img, calibration):
    # This converts the point inside the depth image back to 3d pointcloud.
    u, v = np.where(depth_image > 0)
    z = depth_image[u, v] / 255
    P = calibration['P']
    f_x, f_y, c_x, c_y = P[0, 0], P[1, 1], P[0, 2], P[1, 2]
    x, y = z * (c_x - u) / f_x, z * (c_y - v) / f_y
    pointcloud = np.vstack((x, y, z)).T

    # Use the camera image for coloring
    colors = cam_img[u, v, :]
    rgb_colors = colors * 1.0
    rgb_colors[:, 0] = colors[:, 2] / 256
    rgb_colors[:, 1] = colors[:, 1] / 256
    rgb_colors[:, 2] = colors[:, 0] / 256
    return pointcloud, rgb_colors


def ImageBboxViewer(img, bboxes, classes):
    drawn_img = img.copy()
    if len(classes) > 0:
        for i, bbox in enumerate(bboxes):
            colors = get_colormap()
            color = colors[classes[i]]
            color = (color[2], color[1], color[0])
            start = (np.int(np.round(bbox[0] - bbox[2]/2)), np.int(np.round((bbox[1] - bbox[3]/2))))
            end = (np.int(np.round(bbox[0] + bbox[2]/2)), np.int(np.round((bbox[1] + bbox[3]/2))))
            drawn_img = cv2.rectangle(drawn_img, start, end, color, 2)
    return drawn_img

def LoadCalibrations(calib_path):
    with open(calib_path, 'r') as f:
        custom_calibration = yaml.load(f, Loader=yaml.Loader)
        new_projection_matrix = np.array(custom_calibration['P']).reshape(3, 4)
        custom_intrinsics = {
            'D': np.array(custom_calibration['D']).reshape(1, 5),
            'K': np.array(custom_calibration['K']).reshape(3, 3),
            'P': new_projection_matrix[:3, :3]
            }
    # Generate projection matrices
    return custom_intrinsics

def ShowPointcloud(pointcloud, colors):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(pointcloud)
    pcd.colors = o3d.utility.Vector3dVector(colors)
    o3d.visualization.draw_geometries([pcd],
                                      zoom=0.02,
                                      front=[0.0006742172122467774, 0.28886993227567032, -0.95736811502065422],
                                      lookat=[4.1637300685366672, -0.45651152396463657, 0.039358631742932221],
                                      up=[0.99885624471318879, 0.045576768669417025, 0.014455468000755001])

def euler_matrix(ai, aj, ak, axes='sxyz'):
    _NEXT_AXIS = [1, 2, 0, 1]
    _AXES2TUPLE = {
        'sxyz': (0, 0, 0, 0), 'sxyx': (0, 0, 1, 0), 'sxzy': (0, 1, 0, 0),
        'sxzx': (0, 1, 1, 0), 'syzx': (1, 0, 0, 0), 'syzy': (1, 0, 1, 0),
        'syxz': (1, 1, 0, 0), 'syxy': (1, 1, 1, 0), 'szxy': (2, 0, 0, 0),
        'szxz': (2, 0, 1, 0), 'szyx': (2, 1, 0, 0), 'szyz': (2, 1, 1, 0),
        'rzyx': (0, 0, 0, 1), 'rxyx': (0, 0, 1, 1), 'ryzx': (0, 1, 0, 1),
        'rxzx': (0, 1, 1, 1), 'rxzy': (1, 0, 0, 1), 'ryzy': (1, 0, 1, 1),
        'rzxy': (1, 1, 0, 1), 'ryxy': (1, 1, 1, 1), 'ryxz': (2, 0, 0, 1),
        'rzxz': (2, 0, 1, 1), 'rxyz': (2, 1, 0, 1), 'rzyz': (2, 1, 1, 1)}
    _TUPLE2AXES = dict((v, k) for k, v in _AXES2TUPLE.items())
    try:
        firstaxis, parity, repetition, frame = _AXES2TUPLE[axes]
    except (AttributeError, KeyError):
        _ = _TUPLE2AXES[axes]
        firstaxis, parity, repetition, frame = axes

    i = firstaxis
    j = _NEXT_AXIS[i+parity]
    k = _NEXT_AXIS[i-parity+1]

    if frame:
        ai, ak = ak, ai
    if parity:
        ai, aj, ak = -ai, -aj, -ak

    si, sj, sk = math.sin(ai), math.sin(aj), math.sin(ak)
    ci, cj, ck = math.cos(ai), math.cos(aj), math.cos(ak)
    cc, cs = ci*ck, ci*sk
    sc, ss = si*ck, si*sk

    M = np.identity(4)
    if repetition:
        M[i, i] = cj
        M[i, j] = sj*si
        M[i, k] = sj*ci
        M[j, i] = sj*sk
        M[j, j] = -cj*ss+cc
        M[j, k] = -cj*cs-sc
        M[k, i] = -sj*ck
        M[k, j] = cj*sc+cs
        M[k, k] = cj*cc-ss
    else:
        M[i, i] = cj*ck
        M[i, j] = sj*sc-cs
        M[i, k] = sj*cc+ss
        M[j, i] = cj*sk
        M[j, j] = sj*ss+cc
        M[j, k] = sj*cs-sc
        M[k, i] = -sj
        M[k, j] = cj*si
        M[k, k] = cj*ci
    return M

def quaternion_matrix(quaternion):
    """Return homogeneous rotation matrix from quaternion.

    >>> R = quaternion_matrix([0.06146124, 0, 0, 0.99810947])
    >>> np.allclose(R, rotation_matrix(0.123, (1, 0, 0)))
    True

    """
    q = np.array(quaternion[:4], dtype=np.float64, copy=True)
    nq = np.dot(q, q)
    if nq < np.finfo(float).eps * 4.0:
        return np.identity(4)
    q *= math.sqrt(2.0 / nq)
    q = np.outer(q, q)
    return np.array((
        (1.0-q[1, 1]-q[2, 2],     q[0, 1]-q[2, 3],     q[0, 2]+q[1, 3], 0.0),
        (    q[0, 1]+q[2, 3], 1.0-q[0, 0]-q[2, 2],     q[1, 2]-q[0, 3], 0.0),
        (    q[0, 2]-q[1, 3],     q[1, 2]+q[0, 3], 1.0-q[0, 0]-q[1, 1], 0.0),
        (                0.0,                 0.0,                 0.0, 1.0)
        ), dtype=np.float64)

