import os
import cv2
import sys
import warnings
import argparse
import random
from utils import *
import matplotlib.pyplot as plt
from nuscenes.nuscenes import NuScenes

def main(dataset_root, show, scene_id):
    # Setup paths
    nuscenes_3d_labels = os.path.join(dataset_root, '3d_labels')
    yolo_2d_labels = os.path.join(dataset_root, '2d_labels')
    depth_labels = os.path.join(dataset_root, 'depth_labels')
    depth_labels_color = os.path.join(dataset_root, 'depth_demo')

    # 3D DATA DEMO SETUP
    nusc = NuScenes(version='v1.0-trainval', dataroot=nuscenes_3d_labels, verbose=True)
    nusc.list_scenes()
    nusc.list_categories()
    nusc.list_attributes()
    if scene_id is None:
        scene_id = random.randint(0, 43)
    scene0 = nusc.scene[scene_id]
    log = scene0['log_token']
    sample_token = scene0['first_sample_token']
    calib = LoadCalibrations(os.path.join('calibration', log + '_calibration.yml'))

    print('------------------------------------')
    print('Displaying scene', scene_id, ':', log)
    example_idx = 0
    for i in range(scene0['nbr_samples']):
        while not os.path.exists(os.path.join(nuscenes_3d_labels, 'samples', 'CAM_FRONT', log + "_" + str(example_idx) + '.jpg')):
            example_idx += 1
        sample = nusc.get('sample', sample_token)
        sample_token = sample['next']
        print("Example {} of {}".format(i + 1, scene0['nbr_samples']))
        if show in ['3d', 'all']:
            success = False
            ## TOP VIEW LIDAR PLOT
            axis_limit = 100
            while not success:
                try:
                    nusc.render_sample_data(sample['data']['LIDAR_TOP'], nsweeps=5, axes_limit=axis_limit,
                                            underlay_map=True, verbose=False)
                    success = True
                except:
                    axis_limit = axis_limit - 10

            ## CAMERA VIEW + LIDAR POINTS
            nusc.render_pointcloud_in_image(sample['token'], pointsensor_channel='LIDAR_TOP',
                                            render_intensity=True)

            ## CAMERA VIEW 3D LABELS
            nusc.render_sample_data(sample['data']['CAM_FRONT'])

        ## CAMERA VIEW 2D LABELS
        image = cv2.imread(
            os.path.join(nuscenes_3d_labels, 'samples', 'CAM_FRONT', log + "_" + str(example_idx) + '.jpg'))
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            yolo_labels = np.atleast_2d(
                np.loadtxt(os.path.join(yolo_2d_labels, log + "_" + str(example_idx) + '.pcd.txt')))
        img_labels, classes = Yolo2BBox(image, yolo_labels)
        if show in ['2d', 'all']:
            drawn_img = ImageBboxViewer(image, img_labels, classes)
            drawn_img = cv2.resize(drawn_img, (int(image.shape[1] / 2), int(image.shape[0] / 2)))
            cv2.imshow('yolo_img', drawn_img)
            while cv2.getWindowProperty('yolo_img', cv2.WND_PROP_VISIBLE) >= 1:
                keyCode = cv2.waitKey(1000)
                if (keyCode & 0xFF) == ord("q"):
                    cv2.destroyAllWindows()
                    break

        if show in ['depth', 'all']:
            ## DEPTH IMG + 2D LABELS
            depth_img = cv2.imread(os.path.join(depth_labels_color, log + "_" + str(example_idx) + '.pcd.jpg'))
            drawn_depth_img = ImageBboxViewer(depth_img, img_labels, classes)
            drawn_depth_img = cv2.resize(drawn_depth_img, (int(image.shape[1] / 2), int(image.shape[0] / 2)))
            cv2.imshow('colored_depth_img', drawn_depth_img)

            if show == 'depth':
                # DEPTH IMG, 3D PROJECTION + CAMERA COLORS
                # Depending on your OPEN3D and Matplotlib settings, this might break, so 'depth' only.

                real_depth_img = cv2.imread(os.path.join(depth_labels, log + "_" + str(example_idx) + '.pcd.png'),
                                         cv2.IMREAD_ANYDEPTH)
                depth_pointcloud, depth_colors = DepthImage2Pointcloud(real_depth_img, image, calib)
                ShowPointcloud(depth_pointcloud, depth_colors)

            ## Close all windows properly before next loop
            while cv2.getWindowProperty('colored_depth_img', cv2.WND_PROP_VISIBLE) >= 1:
                keyCode = cv2.waitKey(1000)
                if (keyCode & 0xFF) == ord("q"):
                    cv2.destroyAllWindows()
                    break
            plt.close('all')
        example_idx += 1


if __name__ == '__main__':
    # Parse input
    parser = argparse.ArgumentParser(description='OPERA Signate Dataset Demo')
    parser.add_argument('--input', '-i', type=str, default='dataset/',
                        help='Root folder dataset')
    parser.add_argument('--show', '-s', type=str, default='all',
                        help='What to demo: 2D, 3D, depth, all')
    parser.add_argument('--scene_index', '-x', type=int, default=-1)
    config = parser.parse_args()
    if not os.path.exists(config.input):
        sys.exit("Dataset input path defined by --input was not found: {}".format(config.input))
    if config.show.lower() not in ['2d', '3d', 'depth', 'all']:
        print("Demo option defined --show was not recognized: {}. Default to 'all'".format(config.show))
    if config.scene_index < 0:
        config.scene_index = None
    elif config.scene_index > 43:
        print('There are 44 scenes, scene index should be in [0, 43], not {}. Defaulting to None.'.format(config.scene_index))
        config.scene_index = None


    # Run
    main(config.input, config.show, config.scene_index)

