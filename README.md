## Opera Dataset, 3D (NuScenes format), 2D (YOLO format) and Depth (KITTI format) Labels
<p float="left">
  <img src="docs/cam_front_small.gif" height="300" />
  <img src="docs/lidar_view_small.gif" height="300" />
</p>

### Prerequisites
Setup your Python environment:
```shell
conda env create -f opera_demo.yml
```
If you don't want to use the `conda`, you can install the dependencies via Pip:
```shell
pip install numpy opencv-python==4.2.0.34 nuscenes-devkit open3d pyyaml
```

### Usage
With the dataset folder location defined as `dataset_root`, you can view 3D labels, 2D labels, depth labels or all with the following command:
```shell
python opera_demo.py -i dataset_root --show all --scene_idx 0
```
The possible parameters for `--show` are `2D`, `3D`, `depth` and `all`. Only 'depth' will show the 3D reprojection of camera pixels as matplotlib and Open3D do not cooporate. Otherwise 'all' shows everything. 
scene_idx is **optional**, can be as integers between 0 and 43 inclusively, representing the scene number. If not set, a random scene is chosen.
### Dataset
The dataset (downloaded separately) contains the following files:
```shell
│ signate_dataset
├── 2d_labels/ 
├── 3d_labels/
├── depth_labels/
├── depth_demo/
└── 2d_bbox_demo/
```

The sequence contains 183 pointclouds over 36.4 seconds and features 13941 3D annotations of the following classes and attributes:

Categories list:
```shell
animal                      n=    4, width= 0.19±0.07, len= 0.43±0.22, height= 0.23±0.07, lw_aspect= 2.20±0.58
human.pedestrian.adult      n= 5079, width= 0.56±0.10, len= 0.64±0.22, height= 1.64±0.14, lw_aspect= 1.16±0.38
human.pedestrian.child      n=    3, width= 0.41±0.01, len= 0.25±0.04, height= 1.12±0.08, lw_aspect= 0.61±0.07
human.pedestrian.constructi n=  565, width= 0.55±0.10, len= 0.51±0.16, height= 1.67±0.17, lw_aspect= 0.94±0.32
human.pedestrian.personal_m n=    1, width= 0.72±0.00, len= 0.61±0.00, height= 1.14±0.00, lw_aspect= 0.85±0.00
human.pedestrian.police_off n=    1, width= 0.50±0.00, len= 0.50±0.00, height= 1.50±0.00, lw_aspect= 1.00±0.00
movable_object.barrier      n= 2054, width= 1.22±0.70, len= 2.30±6.61, height= 0.88±0.33, lw_aspect= 2.84±8.70
movable_object.debris       n=   27, width= 0.66±0.90, len= 1.08±1.79, height= 0.29±0.27, lw_aspect= 1.29±0.73
movable_object.pushable_pul n=   59, width= 0.59±0.02, len= 0.59±0.08, height= 0.97±0.10, lw_aspect= 1.00±0.11
movable_object.trafficcone  n= 3869, width= 0.30±0.07, len= 0.25±0.06, height= 0.74±0.07, lw_aspect= 0.89±0.34
static.manmade              n= 6467, width= 0.24±0.04, len= 0.16±0.04, height= 0.72±0.04, lw_aspect= 0.69±0.33
static_object.bicycle_rack  n=   75, width= 6.44±6.69, len= 1.82±0.54, height= 1.21±0.23, lw_aspect= 0.52±0.42
vehicle.bicycle             n= 1398, width= 0.61±0.12, len= 1.75±0.18, height= 1.46±0.29, lw_aspect= 3.00±0.67
vehicle.bus.rigid           n=  407, width= 2.41±0.19, len= 8.60±1.72, height= 2.87±0.30, lw_aspect= 3.54±0.46
vehicle.car                 n=34649, width= 1.74±0.14, len= 4.17±0.55, height= 1.72±0.20, lw_aspect= 2.39±0.24
vehicle.construction        n=  195, width= 1.70±0.28, len= 2.47±0.57, height= 2.65±0.24, lw_aspect= 1.45±0.27
vehicle.emergency.police    n=    6, width= 1.45±0.49, len= 3.98±1.23, height= 1.45±0.29, lw_aspect= 2.89±0.50
vehicle.motorcycle          n=  197, width= 0.79±0.13, len= 1.61±0.09, height= 1.52±0.20, lw_aspect= 2.12±0.61
vehicle.trailer             n=   52, width= 3.02±0.17, len= 8.32±1.72, height= 3.71±0.14, lw_aspect= 2.78±0.71
vehicle.truck               n= 6807, width= 2.41±0.41, len= 7.26±2.19, height= 2.95±0.62, lw_aspect= 2.97±0.57
```

Attributes list:
```
cycle.with_rider: 1078
cycle.without_rider: 788
pedestrian.moving: 4016
pedestrian.sitting_lying_down: 82
pedestrian.standing: 1550
vehicle.moving: 16150
vehicle.parked: 19444
vehicle.stopped: 6447
````

#### 3D Dataset
The 3D dataset is in `Nuscene` format, which can be accessed and manipulated by the [NuScene API](https://github.com/nutonomy/nuscenes-devkit):
```python
nusc = NuScenes(version='v1.0-trainval', dataroot='signate_dataset/3d_labels', verbose=True)
```
Through the API, we can access the 3D labels in both LiDAR frame and camera frame. We can also plot the overall trajectory against the 3D Pointcloud map:
![](docs/ego_poses.png)

#### 2D Dataset
The 2D dataset includes 2D labels corresponding to the `CAM_FRONT` images of the 3D dataset in `YOLO` format. `YOLO` format expresses bounding boxes in normalized image coordinates, such that an object label is expressed as follows:
```text
class_id  x_normalized  y_normalized  width_normalized  height_normalized
```
If a square bounding box of size (width, height) is centered at pixel location (x, y) in an image of size (image_height, image_width), then the normalized coordinates are calculated as follows:
```python
x_normalized = x / image_width
y_normalized = y / image_height
width_normalized = width / image_width
height_normalized = height / image_height
```
The attached python script provides an example function to convert from normalized coordinates to image coordinates. 

During the conversion from LiDAR-based 3D bounding boxes to 2D image boxes, we removed bounding boxes that were:
* More than 80% occluded or out of the image frame,
* Beyond 100m distance from the vehicle,
* Smaller than 10 pixels by 10 pixels in size.

<p float="left">
  <img src="docs/before_nms.jpg" height="300" />
  <img src="docs/after_nms.jpg" height="300" />
</p>

#### Depth Dataset
LiDAR points projected to image allow us to produce the ground truth a depth dataset. The depth labels are encoded as int16 mono grayscale images (colors for demo purposes only), with pixel values ranging from
[0, 65535] representing object depth in camera frame up to a range of 255 meters, where an object at a depth of 0 (or infinity) has pixel intensity 0 and an object at a distance of 255 meters has a pixel intensity of 65535.

The depth image can be converted back to 3D using the camera intrinsics, as shown by the sample code:
```python
z = pixel_intensity / 255
x = z * (c_x - u) / f_x
y = z * (c_y - v) / f_y
```
![](docs/depth_demo.gif)
